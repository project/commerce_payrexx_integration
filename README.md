# Commerce Payrexx integration

Adds integration for the Payrexx payment gateway.

## Installation and basic usage

* Install the module via composer: `composer require drupal/commerce_payrexx_integration`
* Enable the module by running: `drush en commerce_payrexx_integration` or via extend page.
* Configure the module by following the steps:
  * Create the payment gateway under `/admin/commerce/config/payment-gateways`
    * For plugin select `"Payrexx (Redirect to Payrexx)"`
    * Populate required fields (note: one of the ways to get instance name is from the first part of the URL when you log in to the admin panel eg. https://***xxx***.payrexx.com/)
* Extras:
  * Module also provides a checkout pane that allows the user to pre-select the payment method if they so desire.
