<?php

declare(strict_types = 1);

namespace Drupal\commerce_payrexx_integration;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payrexx_integration\Event\PaymentResponseEvent;
use Drupal\commerce_payrexx_integration\Event\PayrexxEvents;
use Drupal\commerce_price\Price;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Payrexx\Models\Request\Gateway;
use Payrexx\Models\Response\Transaction;
use Payrexx\Payrexx;
use Payrexx\PayrexxException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Base service for handling Payrexx transactions.
 */
class TransactionHandlerService {

  use GatewayPluginInstanceGetterTrait;

  public const SUCCESS_RESPONSES = [
    Transaction::CONFIRMED,
  ];

  public const PARTIAL_SUCCESS_RESPONSES = [
    Transaction::INITIATED,
    Transaction::WAITING,
    Transaction::AUTHORIZED,
    Transaction::RESERVED,
  ];

  public const FAILED_RESPONSES = [
    Transaction::DECLINED,
    Transaction::ERROR,
    Transaction::EXPIRED,
    Transaction::INSECURE,
    Transaction::CANCELLED,
    Transaction::UNCAPTURED,
  ];

  public const REFUND_RESPONSES = [
    Transaction::PARTIALLY_REFUNDED,
    Transaction::REFUND_PENDING,
    Transaction::REFUNDED,
  ];

  /**
   * The EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface$entityTypeManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * The logger channel for this module.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * Construct a new TransactionHandlerService.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    EventDispatcherInterface $eventDispatcher,
    LoggerChannelInterface $logger
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->eventDispatcher = $eventDispatcher;
    $this->logger = $logger;
  }

  /**
   * Handle the "return" request (success or error).
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   A commerce order.
   * @param \Payrexx\Payrexx $payrexx_instance
   *   The Payrexx instance (a wrapper around the communicator).
   */
  public function processOrder(OrderInterface $order, Payrexx $payrexx_instance): void {
    $gid = $order->getData('payrexx_gateway_id');
    $gateway = new Gateway();
    $gateway->setId($gid);

    try {
      $response = $payrexx_instance->getOne($gateway);
      $tid = $response->getInvoices()[0]["transactions"][0]["id"];
      $transaction = new Transaction();
      $transaction->setId($tid);
      $transaction_response = $payrexx_instance->getOne($transaction);
      $this->handleResponse($order, $transaction_response);
    }
    catch (PayrexxException $e) {
      $this->logger->error(
        'Error processing transaction for order @order_id. Message: @message.',
        [
          '@order_id' => $order->id(),
          '@message' => $e->getMessage(),
        ]
      );
      throw new PaymentGatewayException();
    }
  }

  /**
   * Handle a successful response.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Payrexx\Models\Response\Transaction $transaction
   *   The transaction response.
   */
  protected function handleResponseSuccess(OrderInterface $order, Transaction $transaction): void {
    $this->dispatchEvent($order, $transaction);
    $this->saveCommercePayment($order, $transaction, 'completed');
  }

  /**
   * Handle a partially successful response.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Payrexx\Models\Response\Transaction $transaction
   *   The transaction response.
   */
  protected function handleResponsePartialSuccess(OrderInterface $order, Transaction $transaction): void {
    $this->logger->info('Received a partially successful payment response for order %order: %details', [
      '%order' => $order->id(),
      '%details' => Json::encode((array) $transaction),
    ]);
    $this->dispatchEvent($order, $transaction);
    $this->saveCommercePayment($order, $transaction, 'completed');
  }

  /**
   * Handle error response.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Payrexx\Models\Response\Transaction $transaction
   *   The transaction response.
   */
  protected function handleResponseError(OrderInterface $order, Transaction $transaction): never {
    $this->logger->warning('Received an error payment response for order %order: %details', [
      '%order' => $order->id(),
      '%details' => Json::encode((array) $transaction),
    ]);
    $this->dispatchEvent($order, $transaction);
    throw new PaymentGatewayException('Payment incomplete or declined');
  }

  /**
   * Get the order ID from the reference ID.
   *
   * @param string $reference_id
   *   The reference ID.
   *
   * @return int
   *   The order ID.
   */
  public function getOrderIdFromReferenceId(string $reference_id): int {
    return (int) $reference_id;
  }

  /**
   * Save the commerce payment.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Payrexx\Models\Response\Transaction $response
   *   The transaction response.
   * @param string $payment_state
   *   The payment state.
   */
  protected function saveCommercePayment(OrderInterface $order, Transaction $response, string $payment_state): void {
    /** @var \Drupal\commerce_payment\PaymentStorage $paymentStorage */
    $paymentStorage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $paymentStorage->loadByRemoteId($response->getId());
    if (!$payment) {
      $payment = $paymentStorage->create(
        [
          'payment_gateway' => $this->getGatewayPluginInstance($order)?->getEntityId() ?? $order->get('payment_gateway')->target_id,
          'order_id' => $order->id(),
          'remote_id' => $response->getId(),
        ]
      );
    }
    $invoice = $response->getInvoice();
    // @todo Check if minor unit conversion correct for all currencies e.g. JPY.
    $amount = new Price((string) ($response->getAmount() / 100), is_array($invoice) ? $invoice['currencyAlpha3'] : $invoice->currency);

    $payment->set('state', $payment_state);
    $payment->set('amount', $amount);
    $payment->set('remote_state', $response->getStatus());
    $payment->save();
  }

  /**
   * Handle the response based on the response status.
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   A commerce order.
   * @param \Payrexx\Models\Response\Transaction $transaction
   *   The post-payment transaction.
   */
  protected function handleResponse(Order $order, Transaction $transaction): void {
    if (in_array($transaction->getStatus(), self::SUCCESS_RESPONSES, TRUE)) {
      $this->handleResponseSuccess($order, $transaction);
    }
    elseif (in_array($transaction->getStatus(), self::PARTIAL_SUCCESS_RESPONSES, TRUE)) {
      $this->handleResponsePartialSuccess($order, $transaction);
    }
    elseif (in_array($transaction->getStatus(), self::FAILED_RESPONSES, TRUE)) {
      $this->handleResponseError($order, $transaction);
    }
    elseif (in_array($transaction->getStatus(), self::REFUND_RESPONSES, TRUE)) {
      $this->logger->warning('Received a currently unhandled payment refund response for order %order: %details', [
        '%order' => $order->id(),
        '%details' => Json::encode((array) $transaction),
      ]);
    }
    else {
      $this->logger->warning('Received a currently unhandled payment response for order %order: %details', [
        '%order' => $order->id(),
        '%details' => Json::encode((array) $transaction),
      ]);
    }
  }

  /**
   * Dispatch the PaymentResponseEvent.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   A commerce order.
   * @param \Payrexx\Models\Response\Transaction $transaction
   *   The post-payment transaction.
   */
  protected function dispatchEvent(OrderInterface $order, Transaction $transaction) {
    $this->eventDispatcher->dispatch(new PaymentResponseEvent($order, $transaction), PayrexxEvents::PAYMENT_RESPONSE);
  }

}
