<?php

declare(strict_types = 1);

namespace Drupal\commerce_payrexx_integration;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payrexx_integration\Plugin\Commerce\PaymentGateway\PayrexxRedirectCheckout;

/**
 * Gets the gateway from an order.
 */
trait GatewayPluginInstanceGetterTrait {

  /**
   * Get the gateway plugin instance.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order form which to get the gateway plugin.
   *
   * @return \Drupal\commerce_payrexx_integration\Plugin\Commerce\PaymentGateway\PayrexxRedirectCheckout|null
   *   A PayrexRedirectCheckout instance or null if no gateway found.
   */
  public function getGatewayPluginInstance(OrderInterface $order): ?PayrexxRedirectCheckout {
    $payment_gateway = $order->get('payment_gateway')->entity;
    if (!$payment_gateway) {
      return NULL;
    }
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    if (!$payment_gateway_plugin instanceof PayrexxRedirectCheckout) {
      return NULL;
    }
    return $payment_gateway_plugin;
  }

}
