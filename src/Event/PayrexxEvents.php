<?php

namespace Drupal\commerce_payrexx_integration\Event;

/**
 * Defines events for the Commerce Payrexx module.
 */
final class PayrexxEvents {

  /**
   * Event fired after receiving a request from Payrexx.
   *
   * @Event
   *
   * @see \Drupal\commerce_payrexx_integration\Event\PaymentResponseEvent
   */
  public const PAYMENT_RESPONSE = 'commerce_payrexx_integration.payment_response';

}
