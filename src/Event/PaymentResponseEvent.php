<?php

namespace Drupal\commerce_payrexx_integration\Event;

use Drupal\commerce_order\Entity\OrderInterface;
use Payrexx\Models\Response\Transaction;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Payment response event.
 *
 * This event is dispatched after receiving the post-payment request from
 * Payrexx. Note that the event is always dispatched, regardless of the
 * payment status (success, error, cancel). The event handler is responsible
 * to check the status and act accordingly.
 *
 * @see \Payrexx\Models\Response
 */
class PaymentResponseEvent extends Event {

  /**
   * PaymentResponseEvent constructor.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The commerce order.
   * @param \Payrexx\Models\Response\Transaction $transaction
   *   The Payrexx response transaction.
   */
  public function __construct(protected readonly OrderInterface $order, protected readonly Transaction $transaction) {
  }

  /**
   * Get the commerce order entity.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The commerce order entity.
   */
  public function getOrder() {
    return $this->order;
  }

  /**
   * Get the Transaction entity.
   *
   * @return \Payrexx\Models\Response\Transaction
   *   The Transaction entity.
   */
  public function getTransaction() {
    return $this->transaction;
  }

}
