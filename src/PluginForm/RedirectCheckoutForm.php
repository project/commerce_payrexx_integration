<?php

namespace Drupal\commerce_payrexx_integration\PluginForm;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Payrexx\Models\Request\Gateway;
use Payrexx\Payrexx;
use Payrexx\PayrexxException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Redirect form for off-site Payrexx payment.
 */
class RedirectCheckoutForm extends BasePaymentOffsiteForm implements ContainerInjectionInterface {

  private const GATEWAY_ALTER_HOOK_NAME = 'commerce_payrexx_integration_gateway';

  /**
   * Construct a new RedirectCheckoutForm.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\commerce_price\MinorUnitsConverterInterface $minorUnitsConverter
   *   The minor units' converter.
   */
  public function __construct(
    protected ModuleHandlerInterface $moduleHandler,
    protected MinorUnitsConverterInterface $minorUnitsConverter,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('module_handler'),
      $container->get('commerce_price.minor_units_converter'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $configuration = $this->getConfiguration();

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $order = Order::load($payment->getOrderId());

    $payment_method = $order->getData('selected_payment_method') && $order->getData('selected_payment_method') !== 'any' ? [$order->getData('selected_payment_method')] : [];

    $payrexx = new Payrexx($configuration['instance_name'], $configuration['secret']);
    $gateway = new Gateway();

    $gateway->setAmount(($payment->getAmount()->getNumber() + ($payment->getAmount()->getNumber() * $configuration['fee'] / 100)) * 100);
    $gateway->setCurrency($payment->getAmount()->getCurrencyCode());
    $gateway->setVatRate($configuration['vat']);
    $gateway->setSuccessRedirectUrl($form['#return_url']);
    $gateway->setCancelRedirectUrl($form['#cancel_url']);
    $gateway->setFailedRedirectUrl($form['#cancel_url']);
    $gateway->setPm($payment_method);
    $gateway->setPreAuthorization(FALSE);
    $gateway->setReservation(FALSE);
    $gateway->setReferenceId($this->createOrderId());
    if ($configuration['send_basket'] == '1') {
      $gateway->setBasket($this->prepareOrderItems($order));
    }

    // Allow altering of the gateway before communicating the order to the
    // remote gateway.
    $this->moduleHandler->alter(
      static::GATEWAY_ALTER_HOOK_NAME,
      $gateway,
      $order,
      $this,
    );
    try {
      $response = $payrexx->create($gateway);
      $order->setData('payrexx_gateway_id', $response->getId());
      $order->save();
    }
    catch (PayrexxException $e) {
      print $e->getMessage();
      exit;
    }

    return $this->buildRedirectForm(
      $form,
      $form_state,
      $response->getLink(),
      [],
      PaymentOffsiteForm::REDIRECT_POST
    );
  }

  /**
   * Build the order id taking order prefix into account.
   *
   * @return string
   *   Return order number.
   */
  protected function createOrderId() {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    $order_id = $payment->getOrderId();

    // Order number must be at least 4 characters.
    // If not, QuickPay will reject the request.
    if (strlen($order_id) < 4) {
      $order_id = substr('000' . $order_id, -4);
    }

    return $order_id;
  }

  /**
   * Returns the gateway configuration.
   *
   * @return array
   *   Returns the configuration array.
   */
  protected function getConfiguration() {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    return $payment_gateway_plugin->getConfiguration();
  }

  /**
   * Prepares the order items for Payrexx gateway.
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   The commerce order entity.
   *
   * @return array
   *   Array of order products.
   */
  protected function prepareOrderItems(Order $order) {
    $order_items = [];
    foreach ($order->getItems() as $item) {
      $item_price = $item->getAdjustedUnitPrice();
      $order_items[] = [
        'name' => [
          1 => $item->getTitle(),
        ],
        'quantity' => (int) $item->getQuantity(),
        'amount' => $item_price->getNumber() * 100,
      ];
    }
    $order_adjustments = $order->getAdjustments();
    foreach ($order_adjustments as $order_adjustment) {
      // To double counting, only include non-zero adjustments that should be
      // displayed separately.
      if (!$order_adjustment->isIncluded() && !$order_adjustment->getAmount()->isZero()) {
        $order_items[] = [
          'name' => [
            1 => $order_adjustment->getLabel(),
          ],
          'quantity' => 1,
          'amount' => $this->minorUnitsConverter->toMinorUnits($order_adjustment->getAmount()),
        ];
      }
    }
    return $order_items;
  }

}
