<?php

namespace Drupal\commerce_payrexx_integration\Controller;

use Drupal\commerce_checkout\CheckoutOrderManagerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\OrderStorageInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payrexx_integration\GatewayPluginInstanceGetterTrait;
use Drupal\commerce_payrexx_integration\TransactionHandlerService;
use Drupal\Component\Serialization\Json;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Payrexx\Models\Response\Transaction;
use Payrexx\Payrexx;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides checkout endpoints for off-site payments.
 */
class WebhookController implements ContainerInjectionInterface {
  use StringTranslationTrait, GatewayPluginInstanceGetterTrait;

  /**
   * The checkout order manager.
   *
   * @var \Drupal\commerce_checkout\CheckoutOrderManagerInterface
   */
  protected $checkoutOrderManager;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerChannelFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The transaction handler service.
   *
   * @var \Drupal\commerce_payrexx_integration\TransactionHandlerService
   */
  protected $transactionHandlerService;

  /**
   * Constructs a new PaymentCheckoutController object.
   *
   * @param \Drupal\commerce_checkout\CheckoutOrderManagerInterface $checkout_order_manager
   *   The checkout order manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   The logger channel factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher.
   * @param \Drupal\commerce_payrexx_integration\TransactionHandlerService $transactionHandlerService
   *   The transaction handler service.
   */
  public function __construct(CheckoutOrderManagerInterface $checkout_order_manager, MessengerInterface $messenger, LoggerChannelFactoryInterface $loggerChannelFactory, EntityTypeManagerInterface $entityTypeManager, EventDispatcherInterface $eventDispatcher, TransactionHandlerService $transactionHandlerService) {
    $this->checkoutOrderManager = $checkout_order_manager;
    $this->messenger = $messenger;
    $this->loggerChannelFactory = $loggerChannelFactory;
    $this->entityTypeManager = $entityTypeManager;
    $this->eventDispatcher = $eventDispatcher;
    $this->transactionHandlerService = $transactionHandlerService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_checkout.checkout_order_manager'),
      $container->get('messenger'),
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
      $container->get('event_dispatcher'),
      $container->get('commerce_payrexx_integration.transaction_handler_service')
    );
  }

  /**
   * The webhook callback from Payrexx.
   */
  public function transactionHandler(Request $request) {
    $transaction_response = Json::decode($request->getContent())['transaction'];
    $transaction = new Transaction();
    $transaction->fromArray($transaction_response);
    $transactionHandlerService = $this->transactionHandlerService;
    $reference_id = $transaction->getReferenceId();
    if (empty($reference_id)) {
      $logger = $this->loggerChannelFactory->get('commerce_payrexx_integration');
      $logger->notice('The transaction ID was not found. This can occur if the webhook test function is triggered from within Payrexx.');
      throw new PaymentGatewayException('Something went wrong processing the payment.');
    }
    $order_storage = $this->entityTypeManager->getStorage('commerce_order');
    assert($order_storage instanceof OrderStorageInterface);
    $order_id = $transactionHandlerService->getOrderIdFromReferenceId($reference_id);
    // Load the unchanged order to avoid refreshing the order.
    $order = $order_storage->loadUnchanged($order_id);
    if (!$order instanceof OrderInterface) {
      $logger = $this->loggerChannelFactory->get('commerce_payrexx_integration');
      $logger->notice('The order (ID: @order_id) could not be loaded for Payrexx webhook. This can happen if a testing environment also calls a production webhook or vice versa.');
      throw new NotFoundHttpException('The requested order was not found.');
    }
    // The transaction data is untrusted to this point. Reload the transaction
    // data from the remote server to ensure the data can be trusted.
    $payrexx_gateway_plugin = $this->getGatewayPluginInstance($order);
    if (!$payrexx_gateway_plugin) {
      throw new NotFoundHttpException();
    }
    $configuration = $payrexx_gateway_plugin->getConfiguration();
    $payrexx_instance = new Payrexx($configuration['instance_name'], $configuration['secret']);
    $this->transactionHandlerService->processOrder($order, $payrexx_instance);
    return new Response('OK', 200);
  }

}
