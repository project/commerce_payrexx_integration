<?php

namespace Drupal\commerce_payrexx_integration\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\Core\Form\FormStateInterface;
use Payrexx\Models\Request\PaymentProvider;
use Payrexx\Payrexx;
use Payrexx\PayrexxException;

/**
 * Provides a payment method selection pane.
 *
 * @CommerceCheckoutPane(
 *   id = "commerce_payrexx_integration_commerce_pane_payment_method_selection",
 *   label = @Translation("Payment method selection pane"),
 *   default_step = "order_information"
 * )
 */
class PaymentMethodSelectionPane extends CheckoutPaneBase {

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $order = $this->order;
    $pane_form['payment_methods'] = [
      '#type' => 'radios',
      '#title' => $this->t('Payment methods'),
      '#description' => $this->t('Choose a payment method now or when you get redirected to payment site'),
      '#options' => [
        'any' => $this->t("I'll choose later"),
      ] + $this->getAvailablePaymentMethods(),
      '#attributes' => [
        'class' => [
          'payment-methods',
        ],
      ],
      '#default_value' => $order->getData('selected_payment_method') ?? 'any',
      '#after_build' => ['commerce_payrexx_integration_process_payment_radios'],
    ];
    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $values = $form_state->getValue($pane_form['#parents']);
    $this->order->setData('selected_payment_method', $values['payment_methods']);
  }

  /**
   * Retrieves all available payment methods from our Payrexx instance.
   *
   * @return array
   *   Available payment methods.
   */
  protected function getAvailablePaymentMethods() {
    $payment_methods = [];

    $config = \Drupal::config('commerce_payment.commerce_payment_gateway.payrexx');

    if (isset($config->get('configuration')['instance_name']) && isset($config->get('configuration')['secret'])) {
      $payrexx = new Payrexx($config->get('configuration')['instance_name'], $config->get('configuration')['secret']);

      $payment_provider = new PaymentProvider();

      try {
        $payment_provider_response = $payrexx->getOne($payment_provider);
        $active_payment_methods = $payment_provider_response->getActivePaymentMethods();
        $payment_methods = array_combine($active_payment_methods, $active_payment_methods);
        foreach ($payment_methods as $key => $payment_method) {
          $payment_methods[$key] = ucfirst(str_replace('-', ' ', (string) $payment_method));
        }
      }
      catch (PayrexxException) {
        return [];
      }
    }

    return $payment_methods;
  }

}
