<?php

declare(strict_types = 1);

namespace Drupal\commerce_payrexx_integration\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payrexx_integration\TransactionHandlerService;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Payrexx\Models\Request\SignatureCheck;
use Payrexx\Payrexx;
use Payrexx\PayrexxException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the Payrexx offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "payrexx_redirect_checkout",
 *   label = @Translation("Payrexx (Redirect to Payrexx)"),
 *   display_label = @Translation("Payrexx"),
 *    forms = {
 *     "offsite-payment" = "Drupal\commerce_payrexx_integration\PluginForm\RedirectCheckoutForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa",
 *   },
 * )
 */
class PayrexxRedirectCheckout extends OffsitePaymentGatewayBase {

  /**
   * Constructs a new PayrexxRedirectCheckout gateway.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $pluginId
   *   The plugin_id for the plugin instance.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $paymentTypeManager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $paymentMethodTypeManager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   The logger channel factory.
   * @param \Drupal\commerce_payrexx_integration\TransactionHandlerService $transactionHandlerService
   *   The transaction handler service.
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, EntityTypeManagerInterface $entityTypeManager, PaymentTypeManager $paymentTypeManager, PaymentMethodTypeManager $paymentMethodTypeManager, TimeInterface $time, protected readonly LoggerChannelFactoryInterface $loggerChannelFactory, protected readonly TransactionHandlerService $transactionHandlerService) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $entityTypeManager, $paymentTypeManager, $paymentMethodTypeManager, $time);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('logger.factory'),
      $container->get('commerce_payrexx_integration.transaction_handler_service')
    );
  }

  /**
   * Get default configuration.
   *
   * @inheritdoc
   */
  public function defaultConfiguration() {
    return [
      'instance_name' => '',
      'secret' => '',
      'vat' => '',
      'fee' => '',
      'send_basket' => TRUE,
    ] + parent::defaultConfiguration();
  }

  /**
   * Get the payment gateway entity ID.
   *
   * @return string
   *   The entity ID.
   */
  public function getEntityId() {
    return $this->entityId;
  }

  /**
   * Build the configuration form.
   *
   * @inheritdoc
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['instance_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Instance name'),
      '#description' => $this->t('This instance name from your Payrexx account.'),
      '#default_value' => $this->configuration['instance_name'],
      '#required' => TRUE,
    ];
    $form['secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret'),
      '#description' => $this->t('The secret from your Payrexx account.'),
      '#default_value' => $this->configuration['secret'],
      '#required' => TRUE,
    ];
    $form['vat'] = [
      '#type' => 'number',
      '#title' => $this->t('VAT'),
      '#min' => 0,
      '#step' => 0.01,
      '#description' => $this->t('The VAT percentage.'),
      '#default_value' => $this->configuration['vat'],
      '#required' => TRUE,
    ];
    $form['fee'] = [
      '#type' => 'number',
      '#title' => $this->t('Fee'),
      '#min' => 0,
      '#step' => 0.01,
      '#description' => $this->t('The Fee percentage.'),
      '#default_value' => $this->configuration['fee'],
      '#required' => TRUE,
    ];
    $form['send_basket'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send "basket" together with order amount'),
      '#description' => $this->t('Send all order items together with order amount (payrexx will calculate item prices in order and will override the amount)'),
      '#default_value' => $this->configuration['send_basket'],
    ];

    return $form;
  }

  /**
   * Check if the secret key and instance field values are valid.
   *
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (isset($values["configuration"]["payrexx_redirect_checkout"]["instance_name"]) && isset($values["configuration"]["payrexx_redirect_checkout"]["secret"])) {
      $instance = $values["configuration"]["payrexx_redirect_checkout"]["instance_name"];
      $secret = $values["configuration"]["payrexx_redirect_checkout"]["secret"];

      $payrexx = new Payrexx($instance, $secret);

      $signature_check = new SignatureCheck();

      try {
        $payrexx->getOne($signature_check);
      }
      catch (PayrexxException $e) {
        $error_message = $this->t('API credentials are incorrect.') . $e->getMessage();
        $form_state->setErrorByName('instance_name', $error_message);
        $this->logger()->error($error_message);
      }
    }
  }

  /**
   * Configuration form submit handler.
   *
   * @inheritdoc
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['instance_name'] = $values['instance_name'];
    $this->configuration['secret'] = $values['secret'];
    $this->configuration['vat'] = $values['vat'];
    $this->configuration['fee'] = $values['fee'];
    $this->configuration['send_basket'] = $values['send_basket'];
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    parent::onReturn($order, $request);
    $configuration = $this->getConfiguration();
    $payrexx_instance = new Payrexx($configuration['instance_name'], $configuration['secret']);
    $this->transactionHandlerService->processOrder($order, $payrexx_instance);
  }

  /**
   * Get the logger channel for this module.
   *
   * @return \Drupal\Core\Logger\LoggerChannelInterface
   *   The logger channel.
   */
  protected function logger() {
    return $this->loggerChannelFactory->get('commerce_payrexx_integration');
  }

}
